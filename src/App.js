import React, { Component } from 'react';
import Header from './containers/Header/Header';
import Portfolio from './containers/Portfolio/Portfolio';
import FeaturedClients from './containers/FeaturedClients/FeaturedClients';
import './App.css';
import './assets/css/normalize.css';
import './assets/css/reset.css';
import './assets/bootstrap/css/bootstrap-grid.min.css';



class App extends Component {



  render() {
    const { hovered } = this.props;
    return (
      <div className="App">
  
          <div className="Yellow"></div>
          <Header/>
          <div className="Yellow"></div>
          <Portfolio hovered={this.props.hovered} />
          <FeaturedClients />
      </div>

    );
  }
}

export default App;
