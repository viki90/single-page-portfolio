import React from 'react';
import './Logo.css';

const menu = (props) => (
    <div className="Logo">
        <div className="Lowend"><p>LOW<span>END</span></p></div>
        <div><p>PORTFOLIO THEME FOR REVIEW</p></div>    
    </div>

);

export default menu;