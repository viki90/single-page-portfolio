import React from 'react';
import Menu from './Menu/Menu';
import SocialIcon from './SocialIcon/SocialIcon';
import Logo from './Logo/Logo';
import './Header.css';

const header = (props) => (
    <div className="Header">
        <div className="MainHeader">
            <div><Logo /></div>
            <div className="SocialNetworkingMenu">
                <SocialIcon />
                <Menu />
            </div>
        </div>
    </div>

);

export default header;