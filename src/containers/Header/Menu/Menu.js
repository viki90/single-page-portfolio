import React from 'react';
import './Menu.css';

const menu = (props) => (
    <div className="Menu">
        <ul>
            <li><a href="/">WORK</a></li>
            <li><a href="/">SERVICES</a></li>
            <li><a href="/">BLOG</a></li>
            <li><a href="/">ABOUT ME</a></li>
            <li><a href="/">CONTACT</a></li>
        </ul>      
    </div>

);

export default menu;
