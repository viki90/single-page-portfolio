import React from 'react';
import './SocialIcon.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faFacebookF, faTwitter, faSkype, faVimeoV, faDribbble, faReact} from '@fortawesome/fontawesome-free-brands';
 
const header = (props) => (
    <div className="SocialIcon">
        <ul>

            <li><a href="/"><FontAwesomeIcon icon={faFacebookF} /></a></li>
            <li><a href="/"><FontAwesomeIcon icon={faTwitter} /></a></li>
            <li><a href="/"><FontAwesomeIcon icon={faDribbble} /></a></li>
            <li><a href="/"><FontAwesomeIcon icon={faVimeoV} /></a></li>
            <li><a href="/"><FontAwesomeIcon icon={faReact} /></a></li>
            <li><a href="/"><FontAwesomeIcon icon={faSkype} /></a></li>
        </ul> 
    </div>

);

export default header;