import React from 'react';
import './Filter.css';

const portfolio = (props) => (
    <div className="Filter">
        <div className="NavFilter">
            <ul>
                <li><a href="#">All Works</a></li>
                <li><a href="#">Web Designing</a></li>
                <li><a href="#">Identity</a></li>
                <li><a href="#">Print</a></li>
                <li><a href="#">Photography</a></li>
                <li><a href="#">Motion</a></li>
            </ul>
        </div>
        <div className="NavButtons">
            <div className="Block">
                <div className="Pic">
                    <div className="Bar1"></div>
                    <div className="Bar2"></div>
                    <div className="Bar3"></div>
                </div>
            </div>
        </div>
    </div>

);

export default portfolio;