import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDribbble } from '@fortawesome/fontawesome-free-brands';
import first from '../../../assets/images/first.jpg';
import second from '../../../assets/images/second.jpg';
import third from '../../../assets/images/third.jpg';
import fourth from '../../../assets/images/fourth.jpg';
import './RowOne.css';


class RowOne extends Component {
  constructor(props) {
    super(props);
    this.hovered = this.hovered.bind(this);
    this.state = { className: 'active NoDisplay' };

  }

  hovered () {
    console.log('hey');
    let className = this.state.className;
    className = 'active';
  /*   this.setState({
      className
    }); */ 
    
    if(this.state.className === 'NoDisplay'){
      this.setState({ className: 'active'})
    }else {
      this.setState({ className: 'NoDisplay'})
    }

  }

  render() {
  
    return (
      <div className="RowOne">
          <div className="RowPic"><img src={first} alt="first"/></div>
          <div className="RowPic"  onClick={this.hovered}>
              <div className={this.state.className} >
                <div className="AwesomeRight">
                  <div className="Awesome"><FontAwesomeIcon icon={faDribbble} /></div>
                  <div className="Awesome"><FontAwesomeIcon icon={faDribbble} /></div>
                </div>
                <div className="ActiveTitle">
                  <h2>PROJECT TITLE (iDENTITY)</h2>
                  <p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                </div>
              </div>
              <img src={second} alt="second"/>
          </div>
          <div className="RowPic">
              <div><img src={third} alt="third" /></div>
              <div><img src={fourth} alt="fourth"/></div>
          </div>
      </div>
    );
  }
}

export default RowOne;