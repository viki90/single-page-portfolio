import React from 'react';
import './Title.css';

const title = (props) => (
    <div className="Title">
        <p>Minimal Responsive Portfolio Theme for</p>
        <p className="Title-second">CREATIVE AGENCY & CREATIVE FOLKS</p>
    </div>

);

export default title;