import React from 'react';
import Title from './Title/Title';
import Filter from './Filter/Filter';
import RowOne from '../Portfolio/RowOne/RowOne';
import RowTwo from '../Portfolio/RowTwo/RowTwo';
import './Portfolio.css';

const portfolio = (props) => (
    <div className="Portfolio">
      <Title />
      <Filter />
      <RowOne hovered={props.hovered} />
      <RowTwo />
    </div>

);

export default portfolio;