import React from 'react';
import fifth from '../../../assets/images/fifth.jpg';
import sixth from '../../../assets/images/sixth.jpg';
import seventh from '../../../assets/images/seventh.jpg';
import eighth from '../../../assets/images/eighth.jpg';
import './RowTwo.css';

const rowOne = (props) => (
  <div className="RowTwo">
    <div className="RowPic">
      <div className="RowPicTop"><img src={sixth} alt="sixth" /></div>
      <div className="RowPicTop">
          <div className="RowPicBottom"><img src={seventh} alt="seventh" /></div>
          <div className="RowPicBottom"><img src={eighth} alt="eighth" /></div>
      </div>
    </div>
    <div className="RowPic"><img src={fifth} alt="fifth" /></div>
</div>

);

export default rowOne;