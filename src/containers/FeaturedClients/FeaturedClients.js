import React, { Component } from 'react';
import Shape from './Shape/Shape';
import Clients from './Clients/Clients';
import './FeaturedClients.css';

class FeaturedClients extends Component {
  render() {
    return (
      <div className="FeaturedClients">
          <Shape />
          <Clients />
      </div>
    );
  }
}

export default FeaturedClients;