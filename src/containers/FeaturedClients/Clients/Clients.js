import React, { Component } from 'react';
import first from '../../../assets/images/SampleLogo1.png';
import second from '../../../assets/images/SampleLogo2.png';
import third from '../../../assets/images/SampleLogo3.png';
import fourth from '../../../assets/images/SampleLogo4.png';
import fifth from '../../../assets/images/SampleLogo5.png';
import './Clients.css';

class Clients extends Component {
    render() {
      return (
        <div className="Clients">
            <div className="ClientsLogo"><img src={first} alt="first"/></div>
            <div className="ClientsLogo"><img src={second} alt="second"/></div>
            <div className="ClientsLogo"><img src={third} alt="third"/></div>
            <div className="ClientsLogo"><img src={fourth} alt="fourth"/></div>
            <div className="ClientsLogo"><img src={fifth} alt="fifth"/></div>
        </div>
      );
    }
  }
  
  export default Clients;