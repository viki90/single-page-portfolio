import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import './Shape.css';

const shape = (props) => (
    <div className="Shape">
        <div className="Start">
            <div className="Line"></div>
            <div className="Line"></div>
        </div>
        <div className="Start">
            <h2>
                <span><FontAwesomeIcon icon={faStar} /></span>
                <span><FontAwesomeIcon icon={faStar} /></span> FEATURED CLIENTS 
                <span><FontAwesomeIcon icon={faStar} /></span>
                <span><FontAwesomeIcon icon={faStar} /></span>
            </h2>
        </div>
        <div className="Start">
            <div className="Line"></div>
            <div className="Line"></div>
        </div>
    </div>

);

export default shape;
